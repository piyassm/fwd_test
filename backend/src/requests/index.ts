import { HEADER, API_REQUEST } from "./config";
import axios from "axios";

export async function callAPIPost(url: string, data: any) {
  try {
    return Promise.all([
      axios({
        url,
        method: "POST",
        headers: HEADER.CONTENT_TYPE_APP_JSON,
        data: JSON.stringify(data),
      }),
    ]);
  } catch (error) {
    return { err: { statusCode: 404 + error } };
  }
}

export async function callAPIGet(url: string) {
  try {
    return Promise.all([
      axios({
        url,
        method: "GET",
        headers: HEADER.CONTENT_TYPE_APP_JSON,
      }),
    ]);
  } catch (error) {
    return { err: { statusCode: 404 + error } };
  }
}

export const serviceGetProduct = async (information: any) => {
  interface bodyRequest {
    [key: string]: string | number;
  }
  const raw: bodyRequest = {
    dob: information.birthdate,
    planCode: information.planCode,
    paymentFrequency: information.paymentFrequency,
  };
  if (information?.gender) {
    raw["genderCd"] = information.gender;
  }
  if (information?.premiumPerYear) {
    raw["premiumPerYear"] = information.premiumPerYear;
  }
  if (information?.saPerYear) {
    raw["saPerYear"] = information.saPerYear;
  }

  try {
    var url = encodeURI(API_REQUEST.GET_PRODUCT);
    /* Cannot Call API */
    // const [RES_API]: any = await callAPIPost(url, raw);
    // return await RES_API.data;
    return {
      quotationProductList: [
        {
          productId: "ECOMMBIG3",
          productTypeCd: "PLAN",
          productFamilyCd: "TERM",
          baseSumAssured: "1205594",
          baseAnnualPremium: "30000",
          productTerm: 5,
          premiumPayingTerm: 5,
          paymentFrequencyCd: "YEARLY",
          planCode: "T11A20",
          selected: true,
        },
      ],
    };
  } catch (error) {
    return { err: { statusCode: 404, error } };
  }
};
