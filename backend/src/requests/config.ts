export const API_CONFIG = {
  MAIN_URL: "https://api.fwd.co.th/dev-ecommerce",
  CONTENT_TYPE_APP_JSON: "application/json",
};

export const HEADER = {
  CONTENT_TYPE_APP_JSON: {
    "Content-Type": API_CONFIG.CONTENT_TYPE_APP_JSON,
  },
};

export const METHOD = {
  POST: "POST",
  GET: "GET",
};

export const API_REQUEST = {
  GET_PRODUCT: API_CONFIG.MAIN_URL + "/getProduct",
};
