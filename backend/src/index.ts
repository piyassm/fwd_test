import express, { Application, Request, Response } from "express";
import cors from "cors";
import { serviceGetProduct } from "./requests";

const app: Application = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.post("/calcProduct", async (req: Request, res: Response) => {
  const result = await serviceGetProduct(req.body);

  res.json(result);
});

app.listen(3001);

console.log(`listening in port 3001`);
