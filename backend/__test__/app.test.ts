import { serviceGetProduct } from "../src/requests";

describe("helpers", function () {
  test("ExpectData CalcProduct", async function () {
    const respone = await serviceGetProduct({});
    const expectData = {
      quotationProductList: [
        {
          productId: "ECOMMBIG3",
          productTypeCd: "PLAN",
          productFamilyCd: "TERM",
          baseSumAssured: "1205594",
          baseAnnualPremium: "30000",
          productTerm: 5,
          premiumPayingTerm: 5,
          paymentFrequencyCd: "YEARLY",
          planCode: "T11A20",
          selected: true,
        },
      ],
    };
    expect(respone).toEqual(expectData);
  });
});
