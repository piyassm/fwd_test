import { createSlice } from "@reduxjs/toolkit";
import * as Strings from "../helpers/allstring";

const initialState = {
  name: {
    value: "",
    name: Strings.ALL_STRING.INPUT_NAME,
  },
  lastname: {
    value: "",
    name: Strings.ALL_STRING.INPUT_LASTNAME,
  },
  gender: {
    value: "",
    name: Strings.ALL_STRING.INPUT_GENDER,
  },
  birthdate: {
    required: true,
    value: "",
    name: Strings.ALL_STRING.INPUT_BIRTHDATE,
  },
  planCode: {
    required: true,
    value: "",
    name: Strings.ALL_STRING.INPUT_PLANCODE,
  },
  premiumPerYear: {
    value: 0,
    name: Strings.ALL_STRING.INPUT_PREMIUMPERYEAR,
  },
  saPerYear: {
    value: 0,
    name: Strings.ALL_STRING.INPUT_SAPERYEAR,
  },
  paymentFrequency: {
    required: true,
    value: "",
    name: Strings.ALL_STRING.INPUT_PAYMENTFREQUENCY,
  },
};

export const InformationSlice = createSlice({
  name: "information",
  initialState,
  reducers: {
    updateName: (state, action) => {
      state.name.value = action.payload ?? "";
    },
    updateLastname: (state, action) => {
      state.lastname.value = action.payload ?? "";
    },
    updateGender: (state, action) => {
      state.gender.value = action.payload ?? "";
    },
    updateBirthdate: (state, action) => {
      state.birthdate.value = action.payload ?? "";
    },
    updatePlanCode: (state, action) => {
      state.planCode.value = action.payload ?? "";
    },
    updatePremiumPerYear: (state, action) => {
      state.premiumPerYear.value = +action.payload;
    },
    updateSaPerYear: (state, action) => {
      state.saPerYear.value = +action.payload;
    },
    updatePaymentFrequency: (state, action) => {
      state.paymentFrequency.value = action.payload ?? "";
    },
    resetInformation: () => initialState,
  },
});

export const {
  updateName,
  updateLastname,
  updateGender,
  updateBirthdate,
  updatePlanCode,
  updatePremiumPerYear,
  updateSaPerYear,
  updatePaymentFrequency,
  resetInformation,
} = InformationSlice.actions;

export default InformationSlice.reducer;
