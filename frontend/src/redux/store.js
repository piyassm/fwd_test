import { configureStore } from "@reduxjs/toolkit";
import information from "./information";

const store = configureStore({
  reducer: { information },
});

export default store;
