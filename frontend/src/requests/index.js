import { HEADER, API_REQUEST } from "./config";
import axios from "axios";

export async function callAPIPost(url, data) {
  try {
    return Promise.all([
      axios({
        url,
        method: "POST",
        headers: HEADER.CONTENT_TYPE_APP_JSON,
        data: JSON.stringify(data),
      }),
    ]);
  } catch (error) {
    return { err: { statusCode: 404 + error } };
  }
}

export async function callAPIGet(url) {
  try {
    return Promise.all([
      axios({
        url,
        method: "GET",
        headers: HEADER.CONTENT_TYPE_APP_JSON,
      }),
    ]);
  } catch (error) {
    return { err: { statusCode: 404 + error } };
  }
}

export const serviceCalcProduct = async (information) => {
  const raw = {
    name: information.name?.value,
    lastname: information.lastname?.value,
    birthdate: information.birthdate?.value,
    planCode: information.planCode?.value,
    premiumPerYear: information.premiumPerYear?.value,
    saPerYear: information.saPerYear?.value,
    paymentFrequency: information.paymentFrequency?.value,
  };
  if (information?.gender?.value) {
    raw["genderCd"] = information.gender.value;
  }

  try {
    var url = encodeURI(API_REQUEST.CALC_PRODUCT);
    const [RES_API] = await callAPIPost(url, raw);
    return await RES_API.data;
  } catch (error) {
    return { err: { statusCode: 404, error } };
  }
};
