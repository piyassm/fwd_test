import React from "react";
import { Form, Input, Button, Radio, Select, DatePicker, Spin } from "antd";
import {
  updateName,
  updateLastname,
  updateGender,
  updateBirthdate,
  updatePlanCode,
  updatePremiumPerYear,
  updateSaPerYear,
  updatePaymentFrequency,
  resetInformation,
} from "./redux/information";
import moment from "moment";
import * as Strings from "./helpers/allstring";
import styled, { css } from "styled-components";
import { useDispatch } from "react-redux";

const DatePickerWrapped = styled(DatePicker)`
  width: 100%;
`;

export const defaultCssInput = css`
  width: calc(100% - 4px);
  margin-bottom: 0;
`;

const FormInputName = styled(Form.Item)`
  ${defaultCssInput};
`;

const FormInputLastName = styled(Form.Item)`
  ${defaultCssInput};
  margin-left: 8px;
`;

const FormInformation = ({ information, submitForm, disabled, loading }) => {
  const dispatch = useDispatch();

  return (
    <>
      <div className="row">
        <div className="col-12">
          <div>
            <Form layout="vertical">
              <Form.Item>
                <div className="d-flex">
                  <FormInputName label={Strings.ALL_STRING.INPUT_NAME}>
                    <Input
                      value={information.name.value}
                      onChange={(e) => dispatch(updateName(e.target.value))}
                      placeholder="กรุณากรอกชื่อ"
                    />
                  </FormInputName>
                  <FormInputLastName label={Strings.ALL_STRING.INPUT_LASTNAME}>
                    <Input
                      value={information.lastname.value}
                      onChange={(e) => dispatch(updateLastname(e.target.value))}
                      placeholder="กรุณากรอกนามสกุล"
                    />
                  </FormInputLastName>
                </div>
              </Form.Item>
              <Form.Item label={Strings.ALL_STRING.INPUT_GENDER}>
                <Radio.Group
                  value={information.gender.value}
                  onChange={(e) => dispatch(updateGender(e.target.value))}
                >
                  <Radio.Button value="">ไม่ระบุ</Radio.Button>
                  <Radio.Button value="MALE">ชาย</Radio.Button>
                  <Radio.Button value="FEMALE">หญิง</Radio.Button>
                </Radio.Group>
              </Form.Item>
              <Form.Item label={Strings.ALL_STRING.INPUT_BIRTHDATE} required>
                <DatePickerWrapped
                  value={
                    information?.birthdate.value
                      ? moment(information?.birthdate.value)
                      : undefined
                  }
                  onChange={(value) =>
                    dispatch(updateBirthdate(value?.format("YYYY-MM-DD")))
                  }
                  placeholder="เลือก วัน เดือน ปี"
                />
              </Form.Item>
              <Form.Item label={Strings.ALL_STRING.INPUT_PLANCODE} required>
                <Select
                  value={information.planCode.value || undefined}
                  onChange={(value) => {
                    dispatch(updatePlanCode(value));
                  }}
                  placeholder="เลือกแบบประกัน"
                >
                  <Select.Option value="T11A20">
                    package 1 (benefit 200k)
                  </Select.Option>
                  <Select.Option value="T11A50">
                    package 2 (benefit 500k)
                  </Select.Option>
                  <Select.Option value="T11AM1">
                    package 3 (benefit 1M)
                  </Select.Option>
                </Select>
              </Form.Item>
              <Form.Item label={Strings.ALL_STRING.INPUT_PREMIUMPERYEAR}>
                <Input
                  value={information.premiumPerYear.value || undefined}
                  onChange={(e) =>
                    dispatch(updatePremiumPerYear(e.target.value))
                  }
                  placeholder="กรุณากรอกเบี้ยประกัน"
                />
              </Form.Item>
              <Form.Item label={Strings.ALL_STRING.INPUT_SAPERYEAR}>
                <Input
                  value={information.saPerYear.value || undefined}
                  onChange={(e) => dispatch(updateSaPerYear(e.target.value))}
                  placeholder="กรุณากรอกทุนประกันภัย"
                />
              </Form.Item>
              <Form.Item
                label={Strings.ALL_STRING.INPUT_PAYMENTFREQUENCY}
                required
              >
                <Select
                  value={information.paymentFrequency.value || undefined}
                  onChange={(value) => {
                    dispatch(updatePaymentFrequency(value));
                  }}
                  placeholder="เลือกงวดการชำระเบี้ย"
                >
                  <Select.Option value="YEARLY">รายปี</Select.Option>
                  <Select.Option value="HALFYEARLY">รายครึ่งปี</Select.Option>
                  <Select.Option value="QUARTERLY">ราย 3 เดือน</Select.Option>
                  <Select.Option value="MONTHLY">รายเดือน</Select.Option>
                </Select>
              </Form.Item>
            </Form>
          </div>
          <div className="text-center">
            <Form.Item>
              <Button type="primary" onClick={submitForm} disabled={disabled}>
                {loading ? <Spin /> : "คำนวณ"}
              </Button>
              <Button
                type="primary"
                danger
                className="ml-3"
                onClick={() => dispatch(resetInformation())}
                disabled={disabled}
              >
                รีเซ็ต
              </Button>
            </Form.Item>
          </div>
        </div>
      </div>
    </>
  );
};

export default FormInformation;
