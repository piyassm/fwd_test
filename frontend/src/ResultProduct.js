import React from "react";
import { Table, Form, Button } from "antd";

const ResultProduct = ({ dataProduct, setShowResult }) => {
  let columns = [];
  if (dataProduct?.quotationProductList?.[0]) {
    Object.keys(dataProduct?.quotationProductList?.[0]).forEach((data, key) => {
      columns.push({
        title: data,
        dataIndex: data,
        key: data,
      });
    });
  }
  let dataSource = [];

  dataProduct?.quotationProductList?.forEach((data, key) => {
    const newData = {
      ...data,
      key,
      selected: data.selected.toString(),
    };
    dataSource.push(newData);
  });

  return (
    <>
      <Table
        columns={columns}
        dataSource={dataSource}
        pagination={false}
        scroll={{ x: 1300 }}
      />
      <div className="text-center">
        <Form.Item>
          <Button
            type="primary"
            danger
            className="ml-3 mt-3"
            onClick={() => setShowResult(false)}
          >
            คำนวณใหม่
          </Button>
        </Form.Item>
      </div>
    </>
  );
};
export default ResultProduct;
