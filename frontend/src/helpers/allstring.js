export const ALL_STRING = {
  INPUT_NAME: "ชื่อ",
  INPUT_LASTNAME: "นามสกุล",
  INPUT_GENDER: "เพศ",
  INPUT_BIRTHDATE: "วันเดือนปีเกิด",
  INPUT_PLANCODE: "แบบประกัน",
  INPUT_PREMIUMPERYEAR: "เบี้ยประกัน",
  INPUT_SAPERYEAR: "ทุนประกันภัย",
  INPUT_PAYMENTFREQUENCY: "งวดการชำระ",
};
