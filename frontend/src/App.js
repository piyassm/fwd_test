import React, { useEffect, useState } from "react";
import FormInformation from "./FormInformation";
import ResultProduct from "./ResultProduct";
import styled from "styled-components";
import { useSelector } from "react-redux";
import { Modal } from "antd";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { serviceCalcProduct } from "./requests";

const AppWrapped = styled.div`
  margin-top: 64px;
  max-width: ${(props) => !props.showResult && "768px"};
`;

const ModalRequired = styled(Modal)`
  .ant-modal-title {
    font-size: 26px;
  }
  .ant-modal-body {
    font-size: 18px;
    p {
      margin-bottom: 0;
    }
  }
`;

const App = () => {
  const [validForm, setValidForm] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [msgPopup, setMsgPopup] = useState([]);
  const [showResult, setShowResult] = useState(false);
  const [dataProduct, setDataProduct] = useState([]);

  const showModal = () => {
    setIsModalVisible(true);
  };
  const handleOk = () => {
    setIsModalVisible(false);
  };

  const information = useSelector((state) => state.information);

  const calcProduct = async () => {

    const data = await serviceCalcProduct(information);

    if (data) {
      setShowResult(true);
      setDataProduct(data);
    }
    setDisabled(false);
    setLoading(false);
    setValidForm(false);
  };

  useEffect(() => {
    if (validForm) {
      setDisabled(true);
      setLoading(true);

      calcProduct();
    }
  }, [validForm]);

  const submitForm = () => {
    const newData = { ...information };
    let passValidStatus = Object.keys(newData).every((data, key) => {
      return newData?.[data]?.required ? newData?.[data]?.value?.length : true;
    });

    let contentPopup = [];
    Object.keys(newData).forEach((data) => {
      if (newData?.[data]?.required && !newData?.[data]?.value?.length) {
        contentPopup.push(`กรุณาระบุ${newData?.[data]?.name}`);
      }
    });

    if (!!contentPopup?.length) {
      showModal();
      setMsgPopup(contentPopup);
      return;
    } else {
      passValidStatus = true;
    }

    if (passValidStatus) {
      setValidForm(true);
    }
  };
  return (
    <>
      <AppWrapped
        showResult={showResult}
        className={showResult ? "container-fluid" : "container"}
      >
        <div className="row">
          <div className="col-12">
            <div className="text-center">
              <h1>คำนวนเบี้ยประกัน</h1>
            </div>
          </div>
        </div>
        {showResult ? (
          <ResultProduct
            dataProduct={dataProduct}
            setShowResult={setShowResult}
          />
        ) : (
          <FormInformation
            information={information}
            submitForm={submitForm}
            disabled={disabled}
            loading={loading}
          />
        )}
        <ModalRequired
          title="แจ้งเตือน"
          visible={isModalVisible}
          onOk={handleOk}
          maskClosable={false}
          cancelButtonProps={{ style: { display: "none" } }}
        >
          {msgPopup?.map((msg, idx) => (
            <div
              style={{ display: "flex", alignItems: "center" }}
              key={`msgPopup-${idx}`}
            >
              <ExclamationCircleOutlined style={{ color: "#d9534f" }} />
              <p style={{ marginLeft: "6px", color: "#d9534f" }}>{msg}</p>
            </div>
          ))}
        </ModalRequired>
      </AppWrapped>
    </>
  );
};

export default App;
