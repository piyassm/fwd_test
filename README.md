# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run (FRONT END):

### `yarn start:front`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

In the project directory, you can run (BACK END):

### `yarn start:back`

Runs the app in the [http://localhost:3001](http://localhost:3001)

Launches the Unit Test:

### `yarn test`

